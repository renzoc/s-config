const environment   = process.env.NODE_ENV || 'development';
try {
	module.exports = require(`${process.cwd()}/config.${environment}.json`);
} catch (e) {
	console.error(`Verifica que el archivo config.${environment}.json en la raíz del proyecto sea válido.`)
	process.exit(1);
}